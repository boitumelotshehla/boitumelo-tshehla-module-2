class AppDetails {
  var _name;
  var _sector;
  var _developer;
  var _year;

  AppDetails() {
    this._name = "Khula ecosystem";
    this._sector = "agriculture";
    this._developer = "Wanda Matandela";
    this._year = "2018";
  }

  void allCaps() {
    print("ALLCAPS: " + _name.toString().toUpperCase());
  }
}

void main() {
  AppDetails app = new AppDetails();

  print("Name of the app: " + app._name);
  print("Sector of the app: " + app._sector);
  print("Name of the Developer: " + app._developer);
  print("Year the app won: " + app._year);
  app.allCaps();
}
